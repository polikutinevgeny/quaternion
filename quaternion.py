import math


class Quaternion:
    def __init__(self, w=0, x=0, y=0, z=0):
        self.data = (w, x, y, z)

    @classmethod
    def from_angles(cls, angle, x, y, z):
        w = math.cos(angle / 2)
        x *= math.sin(angle / 2)
        y *= math.sin(angle / 2)
        z *= math.sin(angle / 2)
        return cls(w, x, y, z)

    @classmethod
    def from_vector(cls, v):
        return Quaternion(0, *v.data)

    def normalize(self):
        q = tuple(n / self.norm() for n in self.data)
        return Quaternion(*q)

    def conjugate(self):
        w, x, y, z = self.data
        return Quaternion(w, -x, -y, -z)

    def norm(self):
        return math.sqrt(sum(n * n for n in self.data))

    def squared_norm(self):
        return sum(n * n for n in self.data)

    def inverse(self):
        return self.conjugate() / self.squared_norm()

    def matrix(self):
        w, x, y, z = self.data
        a = (
            (w, -x, -y, -z),
            (x, w, -z, y),
            (y, z, w, -x),
            (z, -y, x, w)
        )
        return a

    def complex_matrix(self):
        w, x, y, z = self.data
        a = (
            (complex(w, x), complex(y, z)),
            (complex(-y, z), complex(w, -x))
        )
        return a

    def __matmul__(self, other):
        w1, x1, y1, z1 = self.data
        w2, x2, y2, z2 = other.data
        w = w1 * w2 - x1 * x2 - y1 * y2 - z1 * z2
        x = w1 * x2 + x1 * w2 + y1 * z2 - z1 * y2
        y = w1 * y2 + y1 * w2 + z1 * x2 - x1 * z2
        z = w1 * z2 + z1 * w2 + x1 * y2 - y1 * x2
        return Quaternion(w, x, y, z)

    def __mul__(self, other):
        if isinstance(other, Quaternion):
            return sum(map(lambda x: x[0] * x[1], zip(self.data, other.data)))
        else:
            return Quaternion(*map(lambda i: i * other, self.data))

    def __rmul__(self, other):
        return self.__mul__(other)

    def __truediv__(self, other):
        return Quaternion(*map(lambda i: i / other, self.data))

    def __floordiv__(self, other):
        return Quaternion(*map(lambda i: i // other, self.data))

    def __add__(self, other):
        return Quaternion(*map(lambda x: x[0] + x[1], zip(self.data, other.data)))

    def __sub__(self, other):
        return Quaternion(*map(lambda x: x[0] - x[1], zip(self.data, other.data)))

    def __neg__(self):
        return Quaternion(*map(lambda x: -x, self.data))

    def __str__(self):
        return '{} + {}i + {}j + {}k'.format(*self.data)
