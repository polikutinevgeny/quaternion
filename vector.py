import quaternion


class Vector:

    def __init__(self, *args):
        if len(args) != 3:
            raise ValueError
        self.data = args

    def rotate_by_quaternion(self, q):
        vec = quaternion.Quaternion(0, *self.data)
        vec = q @ vec @ q.inverse()
        return Vector(*vec.data[1:])

    def rotate_by_angle(self, angle, x, y, z):
        q = quaternion.Quaternion.from_angles(angle, x, y, z)
        return self.rotate_by_quaternion(q)

    def __getitem__(self, item):
        return self.data[item]

    def __mul__(self, other):
        if isinstance(other, Vector):
            return sum(map(lambda x: x[0] * x[1], zip(self.data, other.data)))
        else:
            return Vector(*map(lambda i: i * other, self.data))

    def __rmul__(self, other):
        return self.__mul__(other)

    def __matmul__(self, other):
        x = self[1] * other[2] - self[2] * other[1]
        y = self[2] * other[0] - self[0] * other[2]
        z = self[0] * other[1] - self[1] * other[0]
        return Vector(x, y, z)

    def __add__(self, other):
        return Vector(*map(lambda x: x[0] + x[1], zip(self.data, other.data)))

    def __sub__(self, other):
        return Vector(*map(lambda x: x[0] - x[1], zip(self.data, other.data)))

    def __neg__(self):
        return Vector(*map(lambda x: -x, self.data))

    def __str__(self):
        return str(self.data)

    def __truediv__(self, other):
        return Vector(*map(lambda i: i / other, self.data))

    def __floordiv__(self, other):
        return Vector(*map(lambda i: i // other, self.data))
