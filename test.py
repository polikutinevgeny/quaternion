from quaternion import *
from vector import *
import unittest


class TestQuaternion(unittest.TestCase):
    def assert_float_tuple(self, got, expected, places=7):
        for val1, val2 in zip(got, expected):
                self.assertAlmostEqual(val1, val2, places)

    def test_init(self):
        v = Quaternion(1, 2, 3, 4)
        self.assertEqual(v.data, (1, 2, 3, 4))

    def test_init_angles(self):
        v = Quaternion.from_angles(math.pi / 2, 1, 1, 1)
        self.assert_float_tuple(v.data, (
            math.sqrt(2) / 2, math.sqrt(2) / 2,
            math.sqrt(2) / 2, math.sqrt(2) / 2))

    def test_init_vector(self):
        vec = Vector(1, 2, 3)
        q = Quaternion.from_vector(vec)
        self.assertEqual(q.data, (0, 1, 2, 3))

    def test_normalize(self):
        v = Quaternion(math.sqrt(2) / 2, math.sqrt(2) / 2, 0, 0)
        self.assertEqual(v.normalize().data,
                         (math.sqrt(2) / 2, math.sqrt(2) / 2, 0, 0))
        v = Quaternion(1, 1, 1, 1)
        self.assertEqual(v.normalize().data,
                         tuple([0.5] * 4))

    def test_conjugate(self):
        v = Quaternion(1, 2, 3, 4)
        self.assertEqual(v.conjugate().data, (1, -2, -3, -4))

    def test_norm(self):
        v = Quaternion(1, 2, 3, 4)
        self.assertEqual(v.norm(), math.sqrt(30))

    def test_squared_norm(self):
        v = Quaternion(1, 2, 3, 4)
        self.assertEqual(v.squared_norm(), 30)

    def test_inverse(self):
        v = Quaternion(1, 1, 1, 1)
        self.assertEqual(v.inverse().data,
                         (1 / 4, -1 / 4, -1 / 4, -1 / 4))

    def test_matrix(self):
        v = Quaternion(1, 2, 3, 4)
        a = (
            (1, -2, -3, -4),
            (2, 1, -4, 3),
            (3, 4, 1, -2),
            (4, -3, 2, 1)
        )
        self.assertEqual(v.matrix(), a)

    def test_cmatrix(self):
        v = Quaternion(1, 2, 3, 4)
        a = (
            (complex(1, 2), complex(3, 4)),
            (complex(-3, 4), complex(1, -2))
        )
        self.assertEqual(v.complex_matrix(), a)

    def test_mult_quat(self):
        v1 = Quaternion(1, 2, 3, 4)
        v2 = Quaternion(5, 6, 7, 8)
        self.assertEqual((v1 @ v2).data, (-60, 12, 30, 24))

    def test_mult_number(self):
        v = Quaternion(1, 2, 3, 4)
        self.assertEqual((v * 2).data, (2, 4, 6, 8))
        self.assertEqual((2 * v).data, (2, 4, 6, 8))

    def test_scal_mult(self):
        v1 = Quaternion(1, 2, 3, 4)
        v2 = Quaternion(5, 6, 7, 8)
        self.assertEqual(v1 * v2, 70)

    def test_truediv(self):
        v = Quaternion(3, 5, 7, 9)
        self.assert_float_tuple((v / 2).data, (1.5, 2.5, 3.5, 4.5))

    def test_floordiv(self):
        v = Quaternion(3, 5, 7, 9)
        self.assertEqual((v // 2).data, (1, 2, 3, 4))

    def test_sub(self):
        v1 = Quaternion(1, 2, 3, 4)
        v2 = Quaternion(5, 6, 7, 8)
        self.assertEqual((v2 - v1).data, (4, 4, 4, 4))

    def test_add(self):
        v1 = Quaternion(1, 2, 3, 4)
        v2 = Quaternion(5, 6, 7, 8)
        self.assertEqual((v2 + v1).data, (6, 8, 10, 12))

    def test_neg(self):
        v1 = Quaternion(1, 2, 3, 4)
        self.assertEqual((-v1).data, (-1, -2, -3, -4))

    def test_str(self):
        v1 = Quaternion(1, 2, 3, 4)
        self.assertEqual(str(v1), '1 + 2i + 3j + 4k')


class TestVector(unittest.TestCase):
    def assert_float_tuple(self, got, expected, places=7):
        for val1, val2 in zip(got, expected):
                self.assertAlmostEqual(val1, val2, places)

    def test_init(self):
        v = Vector(1, 2, 3)
        self.assertEqual(v.data, (1, 2, 3))

    def test_rotate_by_quaternion(self):
        q = Quaternion.from_angles(math.pi / 2, 0, 1, 0)
        v = Vector(1, 1, 1)
        self.assert_float_tuple(v.rotate_by_quaternion(q).data, (1, 1, -1))
        q = Quaternion.from_angles(math.pi, 1, 0, 0)
        self.assert_float_tuple(v.rotate_by_quaternion(q).data, (1, -1, -1))

    def test_rotate_by_vector(self):
        v = Vector(1, 1, 1)
        self.assert_float_tuple(v.rotate_by_angle(math.pi / 2, 0, 1, 0).data, (1, 1, -1))
        self.assert_float_tuple(v.rotate_by_angle(math.pi, 1, 0, 0).data, (1, -1, -1))
        for i in range(9):
            v = v.rotate_by_angle(math.pi / 9, 1, 0, 0)
        self.assert_float_tuple(v.data, (1, -1, -1))
        v2 = Vector(10, 0, 0)
        self.assert_float_tuple(v2.rotate_by_angle(math.pi, 0, 10, 0).data, (-10, 0, 0))

    def test_getitem(self):
        v = Vector(1, 2, 3)
        self.assertEqual(v[2], 3)
        self.assertEqual(v[1:3], (2, 3))

    def test_vec_mult(self):
        v1 = Vector(1, 2, 3)
        v2 = Vector(4, 5, 6)
        self.assertEqual((v1 @ v2).data, (-3, 6, -3))

    def test_num_mult(self):
        v1 = Vector(1, 2, 3)
        self.assertEqual((v1 * 2).data, (2, 4, 6))
        self.assertEqual((2 * v1).data, (2, 4, 6))

    def test_scalar_mult(self):
        v1 = Vector(1, 2, 3)
        v2 = Vector(4, 5, 6)
        self.assertEqual(v1 * v2, 32)

    def test_add(self):
        v1 = Vector(1, 2, 3)
        v2 = Vector(4, 5, 6)
        self.assertEqual((v1 + v2).data, (5, 7, 9))

    def test_sub(self):
        v1 = Vector(1, 2, 3)
        v2 = Vector(4, 5, 6)
        self.assertEqual((v2 - v1).data, (3, 3, 3))

    def test_neg(self):
        v1 = Vector(1, 2, 3)
        self.assertEqual((-v1).data, (-1, -2, -3))

    def test_str(self):
        v1 = Vector(1, 2, 3)
        self.assertEqual(str(v1), '(1, 2, 3)')

    def test_truediv(self):
        v = Vector(3, 5, 7)
        self.assert_float_tuple((v / 2).data, (1.5, 2.5, 3.5))

    def test_floordiv(self):
        v = Vector(3, 5, 7)
        self.assertEqual((v // 2).data, (1, 2, 3))

    def test_exception(self):
        self.assertRaises(ValueError, lambda: Vector(1, 2, 3, 4))


unittest.main()
